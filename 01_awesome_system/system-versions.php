The document you are currently viewing is version <?=$version?>.

<br /><br />
20091123: <a href="/system-1.4.php">Version 1.4</a>
<br />
&nbsp;-&nbsp;updated location of grml.sh
<br />
&nbsp;-&nbsp;updated grml.sh to erase itself so it doesn't get run twice by accident
<br />
&nbsp;-&nbsp;updated smount to modprobe for "aes" module instead of architecture specific "aes_i586"
<br />
&nbsp;-&nbsp;updated addons directory structure to have separate 64-bit and 32-bit packages
<br />
&nbsp;-&nbsp;updated addons to use squashfs instead of cramfs for faster speeds and bigger allotment for packages
<br />
&nbsp;-&nbsp;updated encbkup to exclude unnecessary files
<br />
&nbsp;-&nbsp;updated all scripts to support the 64-bit structure
<br />
&nbsp;-&nbsp;updated my-grml-x to work with newer GRML images

<br /><br />
20090608: <a href="/system-1.3.php">Version 1.3</a>
<br />
&nbsp;-&nbsp;updated my-grml-x to work with newest version of GRML (2009.05)
<br />
&nbsp;-&nbsp;updated the make-addons script to support commenting out lines with "#"

<br /><br />
20080817: <a href="/system-1.2.php">Version 1.2</a>
<br />
&nbsp;-&nbsp;created the script "make-addons" which generates the addon packages cramfs automatically

<br /><br />
20080604: <a href="/system-1.1.php">Version 1.1</a>
<br />
&nbsp;-&nbsp;better system for addon packages

<br /><br />
20080316: <a href="/system-1.0.php">Version 1.0</a>
