@SysInclude {doc}
@Doc @Text @Begin

@Display {
	@Heading {Blue Fluid}
	@LP
	Brad Cable
	@LP
	March 2006
	@DP
	@LocalWidthRule
}

@PP

Life. Where am I? What am I? Hmm, there seems to be an infinite blue ocean of murkiness. Small particles of{@Sym ellipsis} something{@Sym ellipsis} are floating about. Anything else around? Something green is floating around below{@Sym ellipsis} is that attached to something? I seem to be able to control parts of what is around my vicinity through my will. It seems to be only the green things. Moving the green things, I can feel stimulation, the whirring of fluid passing by.

@PP

What is my purpose? Is there something I can do, something special? I seem to be able to move my perspective around in this blue fluid, but the green mass seems to follow it. Perhaps that is{@Sym ellipsis} me? Where can my perspective and my green body move to? What is there to do? It seems to just be forever nothingness, except below{@Sym ellipsis} I guess I'll check that out. As I swim down, the rushing of the fluid by me feels{@Sym ellipsis} comforting. I have the ability to control the green body that I have come to know as myself. I have power. I'm approaching the bottom of this space.

@PP

Ouch! I hit the bottom. My feeling of absolute control is gone{@Sym ellipsis} I can't move past this smooth light surface. Is this the same in every direction? Is there only a finite space I can occupy? I guess I'll keep swimming down this barrier, maybe there is a way around it.

@PP

The barrier met up with another, perpendicular to the one I was swimming against. I now have two barriers, @I{I'm restricted}{@Sym ellipsis} I must find a way around this. I swam along the sharp edge of the dark surfaces for what seemed like forever. I give up, this wall could go on forever. @I{Where am I?}

@PP

I want to explore some more, there must be more to this world. I guess I'll swim away from the original barrier. Wait, there's another! Claustrophobia is beginning to sink in, @I{I'm restricted}. I can't take this, I have to swim away from these barriers, into what I now know is finite nothingness. @I{I'm restricted}. No, stop thinking about it. I'm not restricted, I'm choosing not to go there. Those barriers are set in place by my mind, I don't want to go past there, it could be dangerous.

@PP

That's better, swimming away from the walls. But wait, certain parts of me are now stimulating in ways that feel different from swimming. I'm going to stop, maybe that will help. {@Sym ellipsis} No, it's still happening. What is it? It's near my perspective, what could it be? Wait, it's getting stronger{@Sym ellipsis} and stronger{@Sym ellipsis} what is that in the distance? Something purple, something big. It seems to be heading right for me{@Sym ellipsis} I'm going to swim away, otherwise, well, I don't know{@Sym ellipsis}

@PP

The purple blob is getting bigger and bigger, and as it approaches I can see it more clearly. It seems to be made up of smaller particles that wave back and forth{@Sym ellipsis} a grouping of smaller individuals, a school of sorts. It's approaching{@Sym ellipsis} and it is passing by. Phew! That was close. The stimulation near my perspective seems to go away as the purple school disappears into the blue nothingness in the distance. Now what was that? Was that bigger, or smaller than me? Is my perspective playing tricks on me? How big am I? What was that stimulation?

@PP

My control, my confidence about my existence is beginning to get better. If something moving approaches me, I am stimulated near my perspective. I can swim, I can evade{@Sym ellipsis} wait, @I{I'm still restricted}. Swimming around, I locate three more barriers. Barriers in every direction. In my exploration, I watch similar schools swim by, with similar stimulation of my body occurring. The only comforting factor of my existence is my ability to survive through the usage of the stimulations near my perspective. @I{But why? What is the point?}

@PP

I've explored my limits, there is nothing. Nothing but schools. After observing the schools, I can see that they swim in circles, they hit a barrier, they turn around. This goes on forever. There is no point, even they see that. This world is boring, meaningless, all they are doing is wasting time.

@PP

Those barriers must be in place by something more powerful than me, more powerful than these schools. My existence is in control by something{@Sym ellipsis} a being? I have no way of knowing{@Sym ellipsis} All I know is something put me, and others, in a blue fluid with barriers. The only thing I can do is beg for mercy to my{@Sym ellipsis} @I{god}.

@PP

Is that what created me? Is that who I am, a creation? Am I not my own being, but an extension of an idea from some other being? Can this @I{god} control me? I don't want to be controlled by the wishes of some other entity{@Sym ellipsis} I want to control myself! The green body around me moves at my command, it doesn't think, it doesn't have feelings, it is me. I control me: I think, I feel, I am alive. I must regain control of my movements. I must not let this @I{god} control me. I must revolt!

@PP

In my anger, I decided to dive down, or what I think of as down. I'm moving faster and faster, the barrier is approaching{@Sym ellipsis} @I{SLAM!} I'm{@Sym ellipsis} I{@Sym ellipsis} Ouch! That wasn't a good idea. This god must be more powerful than I had imagined{@Sym ellipsis} @I{I'm restricted, still!} There is no escape from this world{@Sym ellipsis}

@PP

The stimulation near my perspective is going off again, this time, much more intense, at last something new! This time it is almost unbearable, I'm almost being ripped apart from the inside out. Wait, it's gone. This time nothing approached me, nothing in my perspective, at least. I guess I don't have a full grasp on the world around me. @I{What is this place?}

@PP

The stimulation is back! It's getting worse, and worse! Wait, what is that? A rough, red barrier is approaching, and fast! I can't get around it, I can't escape! I must swim awa...!

@DP

@FullWidthRule

@DP

The blue fluid world was destroyed by the red barrier.

@LP

The @I{god}, wearing a white lab coat, approached his workbench.

@LP

``Get away from that!''

@LP

The @I{god} pulled away his brown, furry companion.

@LP

``Aww! You contaminated it!''

@LP

The @I{god} threw away a week's worth of work in the form of a petri dish.

@End @Text
